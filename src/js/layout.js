import helper from './helper';
import SimpleBar from 'simplebar';
var Flickity = require('flickity');

// ------ |
// HEADER |
// ------ |

const xHeader                = document.querySelector('.x-hdr');
const xHeaderBackgroundColor = helper.xGetStyle(xHeader, 'backgroundColor');
const xSidebarToggle         = xHeader.querySelector('.x-sbr-tgl');
const xHeaderLogoWrapper     = xHeader.querySelector('.x-logo-wpr');
const xLogo                  = xHeader.querySelector('.x-logo');
const xHeaderNavWrapper      = xHeader.querySelector('.x-hdr-nav-wpr');
const xHeaderList            = xHeader.querySelector('.x-hdr-lst');
const xHeaderNavItems        = Array.from(xHeaderList.getElementsByClassName('x-itm'));
const navItemHoverColor      = helper.xShadeColor(xHeaderBackgroundColor, -0.2);
const navItemActiveColor     = helper.xShadeColor(xHeaderBackgroundColor, -0.3);
const xStreams               = Array.from(document.getElementsByClassName('x-stream'));
const xDdBoxes               = Array.from(document.getElementsByClassName('x-dd-box'));
const xHeaderNotifications   = Array.from(xHeaderList.getElementsByClassName('x-ups'));
const flktHeader = new Flickity(xHeaderList, {
    prevNextButtons: false,
    pageDots: false,
    contain: true,
    freeScroll: true,
    rightToLeft: true,
    cellAlign: 'right',
});

xHeaderLogoWrapper.style.background = xHeaderBackgroundColor;
xSidebarToggle.style.background = xHeaderBackgroundColor;

xHeaderNotifications.forEach((item) => {
    item.style.display = 'block';
});

// ------- |
// SIDEBAR |
// ------- |

const xSidebar                        = document.querySelector('.x-sbr');
let xSidebarOpen                      = true;
const xSidebarBackgroundColor         = helper.xGetStyle(xSidebar, 'backgroundColor');
const xSidebarHeader                  = xSidebar.querySelector('.x-sbr-hdr');
const xSidebarNav                     = xSidebar.querySelector('.x-sbr-nav');
const xSidebarItems                   = Array.from(xSidebarNav.getElementsByClassName('resource'));
const xSidebarSubItems                = Array.from(xSidebarNav.getElementsByClassName('x-sub'));
const xSidebarActiveNavItem           = xSidebarNav.querySelector('.x-itm.x-acv');
const xSidebarTitleItems              = Array.from(xSidebarNav.getElementsByClassName('x-title'));

applyBackgroundColorsToSidebarSubItemsChildren(xSidebarNav, 'x-sub', xSidebarBackgroundColor);

// -------------- |
// SIDEBAR HEADER |
// -------------- |

let xSidebarHeaderItems = [];
let xSidebarControl = null;
let xSidebarHeaderControlItemActive = null;
let xSidebarControlItems = [];
let xSidebarControlTargetItems = [];

if (xSidebarHeader !== null && xSidebarHeader !== undefined) {
    xSidebarHeaderItems             = Array.from(xSidebarHeader.children).filter(item => item.classList.contains('x-itm'));
    xSidebarControl                 = xSidebarHeader.querySelector('.x-sbr-ctrl');
    xSidebarHeaderControlItemActive = xSidebarControl.querySelector('.x-tgl.x-acv');
    xSidebarControlItems            = Array.from(xSidebarControl.getElementsByClassName('x-itm'));
    xSidebarControlTargetItems      = Array.from(xSidebarControl.getElementsByClassName('x-tgl'));
}

if (xSidebarControl !== null && xSidebarControl !== undefined) {
    xSidebarControl.style.backgroundColor = helper.xShadeColor(xSidebarBackgroundColor, 0.1);
}

xSidebarHeaderItems.forEach(item => item.style.backgroundColor = helper.xShadeColor(xSidebarBackgroundColor, 0.2));
xSidebarHeaderItems.forEach(item => helper.xHide(item));

if (xSidebarHeaderControlItemActive) {
    xSidebarHeaderControlItemActive.style.backgroundColor = helper.xShadeColor(xSidebarBackgroundColor, 0.2);

    let activeItemId = xSidebarHeaderControlItemActive.dataset.target;
    let activeItem   = document.getElementById(activeItemId);

    helper.xShow(activeItem);
}

const xSidebarToggleItems = document.querySelector('.x-tgl-sbr-itms');
let xSidebarToggleUp = null;
let xSidebarToggleDown = null;

if (xSidebarToggleItems) {
    xSidebarToggleUp   = xSidebarToggleItems.querySelector('.x-tgl-up');
    xSidebarToggleDown = xSidebarToggleItems.querySelector('.x-tgl-down');
}

// ---------------- |
// SCROLLABLE AREAS |
// ---------------- |

const simpleBar = new SimpleBar(xSidebarNav);
const scrollableElements = document.querySelectorAll('.scrollable');

scrollableElements.forEach(item => {
    new SimpleBar(item);
});

const simpleBarContentWrapperElements = Array.from(document.getElementsByClassName('simplebar-content-wrapper'));

// ------- |
// CONTENT |
// ------- |

const xContentWrapper = document.querySelector('.x-content-wrapper');

// ------- |
// METHODS |
// ------- |

function xSidebarOpenSubItems() {
    return xSidebarSubItems.filter(item => item.classList.contains('x-sidebar-sub-nav-open'));
}

function xSidebarHasClassClosed() {
    return xSidebar.classList.contains('x-sidebar-closed');
}

function xSetSidebarClosed() {
    xSidebarOpen = false;
}

function xSetSidebarOpen() {
    xSidebarOpen = true;
}

function xSidebarIsClosed() {
    return !xSidebarOpen;
}

function xSidebarNavShouldExpand() {
    return xSidebarNav.classList.contains('x-expand');
}

function xSidebarIsExpanded() {
    return xSidebarNav.classList.contains('x-expand');
}

function xSetSidebarNavHeight() {
    let result = helper.viewportHeight() - xHeader.offsetHeight;

    if (xSidebarHeader) {
        result -= xSidebarHeader.offsetHeight
    }

    xSidebarNav.style.height = result + 'px';
}

function applyBackgroundColorsToSidebarSubItemsChildren(element, className, baseColor) {
    for (let child of element.children)  {
        Array.from(child.children).forEach(item => {
            if (item.classList.contains(className)) {
                const shade = helper.xShadeColor(baseColor, 0.1);
                item.style.backgroundColor = shade;
                applyBackgroundColorsToSidebarSubItemsChildren(item, className, shade);
            }
        });
    }
}

module.exports = {
    xHeaderList: xHeaderList,
    xSidebarToggle: xSidebarToggle,
    xHeaderLogoWrapper: xHeaderLogoWrapper,
    xHeaderNavWrapper: xHeaderNavWrapper,
    xHeaderNavItems: xHeaderNavItems,
    navItemHoverColor: navItemHoverColor,
    navItemActiveColor: navItemActiveColor,
    xStreams: xStreams,
    xDdBoxes: xDdBoxes,
    xSidebar: xSidebar,
    xSidebarBackgroundColor: xSidebarBackgroundColor,
    xSidebarHeader: xSidebarHeader,
    xSidebarHeaderItems: xSidebarHeaderItems,
    xSidebarHeaderControlItemActive: xSidebarHeaderControlItemActive,
    xSidebarControlItems: xSidebarControlItems,
    xSidebarControlTargetItems: xSidebarControlTargetItems,
    xSidebarToggleItems: xSidebarToggleItems,
    xSidebarToggleUp: xSidebarToggleUp,
    xSidebarToggleDown: xSidebarToggleDown,
    xSidebarNav: xSidebarNav,
    xSidebarItems: xSidebarItems,
    xSidebarSubItems: xSidebarSubItems,
    xSidebarActiveNavItem: xSidebarActiveNavItem,
    xContentWrapper: xContentWrapper,
    xSidebarTitleItems: xSidebarTitleItems,
    xSidebarOpenSubItems: xSidebarOpenSubItems,
    xSidebarIsClosed: xSidebarIsClosed,
    xSidebarNavShouldExpand: xSidebarNavShouldExpand,
    xSidebarIsExpanded: xSidebarIsExpanded,
    xSetSidebarClosed: xSetSidebarClosed,
    xSetSidebarOpen: xSetSidebarOpen,
    xSidebarHasClassClosed: xSidebarHasClassClosed,
    xSetSidebarNavHeight: xSetSidebarNavHeight,
    flktHeader: flktHeader,
    simpleBar: simpleBar,
    simpleBarContentWrapperElements: simpleBarContentWrapperElements,
};
