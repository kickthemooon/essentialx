import helper from './helper';
import layout from './layout';

window.addEventListener('resize', function () {
    if (!helper.xIsMobile()) {
        helper.xShow(layout.xHeaderNavWrapper);
    }

    if (helper.xIsMobile()) {
        if (!layout.xSidebarIsClosed()) {
            helper.xHide(layout.xHeaderNavWrapper);
        }
    }

    layout.xSetSidebarNavHeight();
    layout.simpleBarContentWrapperElements.forEach(item => {
        item.style.paddingRight = '0px';
    });
});
