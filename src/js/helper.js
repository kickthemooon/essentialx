function xFindAncestor(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;

}

function xFindChildByTagAndClassName(element, tag, className) {
    return Array.from(element.getElementsByTagName(tag))
        .find(child => {
            return child.className.includes(className);
        });
}

function xShowAncestors(item, ancestorClass) {
    if (item) {
        let ancestor = xFindAncestor(item, ancestorClass);

        if (ancestor) {
            xShow(ancestor);
            ancestor.classList.add('x-sidebar-sub-nav-open');
            xShowAncestors(ancestor, ancestorClass)
        }
    }
}

function xExchangeClass(element, classRemove, classAdd) {
    element.classList.remove(classRemove);
    element.classList.add(classAdd);
}

function xChangeBackground(item, color) {
    item.style.backgroundColor = color;
}

function xIsMobile() {
    return window.innerWidth <= 480;
}

function xIsTablet() {
    return window.innerWidth > 768 && window.innerWidth < 1023;
}

function xIsDesktop() {
    return window.innerWidth > 1023;
}

function xShadeColor(color, percent) {
    if (!xIsRGBColor(color)) {
        color = xConvertHexToRGB(color);
    }

    let f = color.split(',');
    let t = percent < 0 ? 0 : 255;
    let p = percent < 0 ? percent * -1 : percent;
    let R = parseInt(f[0].slice(4));
    let G = parseInt(f[1]);
    let B = parseInt(f[2]);

    return "rgb(" + (Math.round((t - R) * p) + R) + "," + (Math.round((t - G) * p) + G) + "," + (Math.round((t - B) * p) + B) + ")";
}

function xGetStyle(el, styleProp) {
    if (el.currentStyle) return el.currentStyle[styleProp];

    return document.defaultView.getComputedStyle(el, null)[styleProp];
}

function xIsRGBColor(color) {
    return color.includes('rgb');
}

function xConvertHexToRGB(hexColor) {
    let hex = hexColor.replace('#', '');

    let r = parseInt(hex.substring(0, 2), 16);
    let g = parseInt(hex.substring(2, 4), 16);
    let b = parseInt(hex.substring(4, 6), 16);

    return 'rgb(' + r + ',' + g + ',' + b + ')';
}

function xHide(item) {
    if (item) item.style.display = 'none';
}

function xShow(item) {
    if (item) item.style.display = 'block';
}

function toggleDisplay(item) {
    if (xGetStyle(item, 'display') === 'none') {
        xShow(item);
    } else {
        xHide(item);
    }
}

function viewportHeight() {
    return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
}

module.exports = {
    xShowAncestors: xShowAncestors,
    xChangeBackground: xChangeBackground,
    xIsMobile: xIsMobile,
    xIsTablet: xIsTablet,
    xIsDesktop: xIsDesktop,
    xShadeColor: xShadeColor,
    xGetStyle: xGetStyle,
    xHide: xHide,
    xShow: xShow,
    toggleDisplay: toggleDisplay,
    viewportHeight: viewportHeight
};
