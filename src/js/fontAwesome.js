import fontawesome from "@fortawesome/fontawesome";
import fasAngleUp from "@fortawesome/fontawesome-free-solid/faAngleUp";
import fasAngleDown from "@fortawesome/fontawesome-free-solid/faAngleDown";
import fasSearch from "@fortawesome/fontawesome-free-solid/faSearch";
import fasUser from "@fortawesome/fontawesome-free-solid/faUser";
import fasBars from "@fortawesome/fontawesome-free-solid/faBars";
import fasDotCircle from "@fortawesome/fontawesome-free-solid/faCircle";
import fasCircle from "@fortawesome/fontawesome-free-solid/faDotCircle";
import fasBell from "@fortawesome/fontawesome-free-solid/faBell";
import fasCommentAlt from "@fortawesome/fontawesome-free-solid/faCommentAlt";
import fasTimes from "@fortawesome/fontawesome-free-solid/faTimes";
import fasNews from "@fortawesome/fontawesome-free-solid/faNewspaper";
import fasImages from "@fortawesome/fontawesome-free-solid/faImages";
import fasTachometer from "@fortawesome/fontawesome-free-solid/faTachometerAlt";
import fasTrash from "@fortawesome/fontawesome-free-solid/faTrash";
import fasSignOutAlt from "@fortawesome/fontawesome-free-solid/faSignOutAlt";
import fasChevronCircleDown from "@fortawesome/fontawesome-free-solid/faChevronCircleDown";

fontawesome.library.add(
    fasAngleUp,
    fasAngleDown,
    fasSearch,
    fasUser,
    fasBars,
    fasCircle,
    fasDotCircle,
    fasBell,
    fasCommentAlt,
    fasTimes,
    fasNews,
    fasImages,
    fasTachometer,
    fasTrash,
    fasSignOutAlt,
    fasChevronCircleDown
);