import helper from './helper';
import layout from './layout';

layout.xHeaderNavItems.forEach(
    item => item.addEventListener('mouseenter', () => itemMouseOverBackground(item))
);

layout.xHeaderNavItems.forEach(
    item => item.addEventListener('mouseleave', () => itemMouseLeaveBackground(item))
);

layout.xHeaderNavItems.forEach(
    liItem => liItem.addEventListener('click', () => toggleItem(liItem))
);

window.addEventListener('resize', function () {
    if (!helper.xIsMobile()) {
        layout.flktHeader.resize();
        repositionDdBox();
    }

    if (helper.xIsMobile()) {
        repositionDdBox();
    }
});

function repositionDdBox() {
    const activeLiElement = layout.xHeaderList.querySelector('.x-itm.x-acv');
    if (activeLiElement === null || activeLiElement === undefined) {
        return;
    }
    const ddBox = document.getElementById(activeLiElement.dataset.ddBoxId);
    if (ddBox === null || ddBox === undefined) {
        return;
    }

    positionDdBox(activeLiElement, ddBox);
}

function toggleItem(liItem) {
    hideUpdates(liItem);
    toggleActive(liItem);
    toggleStream(liItem);
    toggleDdBox(liItem);
}

function toggleDdBox(item) {
    if (! hasDdBox(item) || ! item.classList.contains('x-acv')) {
        layout.xDdBoxes.forEach(
            otherDdBox => {
                otherDdBox.style.left = -1000 + 'px';
            }
        );

        return;
    }

    const ddBox = document.getElementById(item.dataset.ddBoxId);

    layout.xDdBoxes.forEach(
        otherDdBox => {
            if (otherDdBox !== ddBox) {
                otherDdBox.style.left = -1000 + 'px';
            }
        }
    );

    positionDdBox(item, ddBox);
}

function positionDdBox(item, ddBox) {
    if (helper.xIsMobile()) {
        ddBox.style.left = '0px';
        return;
    }
    const itemRect = item.getBoundingClientRect();
    const centerH = itemRect.left + (itemRect.right - itemRect.left) / 2;
    const ddBoxWidth = ddBox.offsetWidth;
    const left = centerH - ddBoxWidth / 2;
    const windowWidth = window.innerWidth;
    const difference = left + ddBoxWidth - windowWidth;

    if (difference > 0) {
        const differenceWithPadding = left - (difference + 40);
        ddBox.style.left = differenceWithPadding + 'px';
    } else {
        ddBox.style.left = left + 'px';
    }
}

function toggleActive(item) {
    layout.xHeaderNavItems.forEach(
        otherItem => {
            if (otherItem !== item) {
                otherItem.classList.remove('x-acv');
                helper.xChangeBackground(otherItem, '');
            } else {
                toggleClassActive(item)
            }
        }
    );

    helper.xChangeBackground(item, layout.navItemActiveColor);
}

function toggleClassActive(item) {
    if (hasChild(item)) {
        if (!isActive(item)) {
            item.classList.add('x-acv');
        } else {
            item.classList.remove('x-acv');
        }
    }
}

function hasChild(item) {
    return hasStream(item) || hasDdBox(item);
}

function toggleStream(item) {
    if (! hasStream(item)) {
        layout.xStreams.forEach(
            otherStream => {
                helper.xHide(otherStream);
            }
        );

        return;
    }

    const stream = document.getElementById(item.dataset.streamId);

    layout.xStreams.forEach(
        otherStream => {
            if (otherStream !== stream) {
                helper.xHide(otherStream);
            }
        }
    );

    helper.toggleDisplay(stream);
}

function hideUpdates(item) {
    let updates = item.querySelector('.x-ups');

    if (updates) {
        helper.xHide(updates);
    }
}

function itemMouseOverBackground(item) {
    if (!item.classList.contains('x-acv')) {
        helper.xChangeBackground(item, layout.navItemHoverColor);
    }
}

function itemMouseLeaveBackground(item) {
    if (!item.classList.contains('x-acv')) {
        helper.xChangeBackground(item, '');
    }
}

function isActive(item) {
    return item.classList.contains('x-acv');
}

function hasStream(item) {
    const stream = document.getElementById(item.dataset.streamId);

    return stream !== null && stream !== undefined;
}

function hasDdBox(item) {
    const ddBox = document.getElementById(item.dataset.ddBoxId);

    return ddBox !== null && ddBox !== undefined;
}
