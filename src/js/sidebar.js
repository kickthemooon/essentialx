import helper from './helper';
import layout from './layout';
import sidebarToggle from './sidebarToggle';

// ----- |
// SETUP |
// ----- |

if (helper.xIsMobile()) {
    sidebarToggle.xOpenSidebar(false);
    layout.xSetSidebarClosed();
}

layout.xSetSidebarNavHeight();

if (layout.xSidebarNavShouldExpand()) {
    xExpandSidebarItems();
} else {
    xCollapseSidebarItems();
}

helper.xShowAncestors(layout.xSidebarActiveNavItem, 'x-sub');

layout.flktHeader.resize();

// ------ |
// EVENTS |
// ------ |
layout.xSidebarItems.forEach(item => item.addEventListener('click', () => xToggleSidebarNavItemOpen(item)));

// sidebar control items hover
layout.xSidebarControlItems
    .forEach(item => item
        .addEventListener('mouseenter', () => xSidebarControlItemMouseEnter(item)));
layout.xSidebarControlItems
    .forEach(item => item
        .addEventListener('mouseleave', () => xSidebarControlItemMouseLeave(item)));

layout.xSidebarControlTargetItems
    .forEach(item => item
        .addEventListener('click', () => xToggleControlItemTarget(item)));

layout.xSidebarToggle.addEventListener('click', () => xToggleSidebar());

if (layout.xSidebarToggleItems) {
    layout.xSidebarToggleItems.addEventListener('click', () => xToggleSidebarItems());
}

// ------- |
// CONTROL |
// ------- |
function xToggleSidebarItems() {
    if (layout.xSidebarIsExpanded()) {
        xCollapseSidebarItems();
    } else {
        xExpandSidebarItems();
    }
}

function xExpandSidebarItems() {
    layout.xSidebarSubItems.forEach(item => {
        helper.xShow(item);
        item.classList.add('x-sidebar-sub-nav-open');
    });
    helper.xShow(layout.xSidebarToggleUp);
    helper.xHide(layout.xSidebarToggleDown);
    layout.xSidebarNav.classList.add('x-expand');
}

function xCollapseSidebarItems() {
    layout.xSidebarSubItems.forEach(item => {
        helper.xHide(item);
        item.classList.remove('x-sidebar-sub-nav-open');
    });
    helper.xHide(layout.xSidebarToggleUp);
    helper.xShow(layout.xSidebarToggleDown);
    layout.xSidebarNav.classList.remove('x-expand');
}

function xToggleSidebar() {
    if (!layout.xSidebarIsClosed()) {
        layout.xSetSidebarClosed();
        sidebarToggle.xOpenSidebar(false);
    } else {
        layout.xSetSidebarOpen();
        sidebarToggle.xOpenSidebar(true);
    }

    layout.flktHeader.resize();
}

function xToggleSidebarNavItemOpen(mainItem) {
    if (!layout.xSidebarHasClassClosed()) {
        const subNavItem = mainItem.parentElement.querySelector('.x-sub');
        if (subNavItem !== null && subNavItem !== undefined) {
            subNavItem.classList.toggle('x-sidebar-sub-nav-open');
            helper.toggleDisplay(subNavItem);
        }
    }
}

function xToggleControlItemTarget(item) {
    layout.xSidebarHeaderItems.forEach(otherItem => helper.xHide(otherItem));

    layout.xSidebarControlTargetItems.forEach(item => item.classList.remove('x-acv'));

    layout.xSidebarControlTargetItems.forEach(
        item => item.style.backgroundColor = helper.xShadeColor(layout.xSidebarBackgroundColor, 0.1)
    );

    item.classList.add('x-acv');
    item.style.backgroundColor = helper.xShadeColor(layout.xSidebarBackgroundColor, 0.2);

    let activeItemId = item.dataset.target;
    let activeItem   = document.getElementById(activeItemId);

    helper.xShow(activeItem);

    layout.xSetSidebarNavHeight();
}

function xSidebarControlItemMouseEnter(item) {
    if (!item.classList.contains('x-acv'))
        helper.xChangeBackground(item, helper.xShadeColor(layout.xSidebarBackgroundColor, 0.05))
}

function xSidebarControlItemMouseLeave(item) {
    if (!item.classList.contains('x-acv'))
        helper.xChangeBackground(item, helper.xShadeColor(layout.xSidebarBackgroundColor, 0.1))
}
