import helper from './helper';
import layout from './layout';

let hover = false;

layout.xSidebar.addEventListener('mouseenter', () => {
    hover = true;
    if (layout.xSidebarIsClosed()) {
        setTimeout(function () {
            if (hover) xOpenSidebar(true);
        }, 250);
    }
});

layout.xSidebar.addEventListener('mouseleave', () => {
    hover = false;
    if (layout.xSidebarIsClosed()) {
        setTimeout(function () {
            if (! hover) xOpenSidebar(false);
        }, 250);
    }
});

function xOpenSidebar(flag) {
    xOpenLogo(flag);

    if (helper.xIsMobile()) {
        xHideHeaderNav(flag);
    }

    if (layout.xSidebarIsClosed()) {
        xOpenContentFull(false);
    } else {
        xOpenContentFull(flag);
    }

    if (flag) {
        layout.xSidebar.classList.remove('x-sidebar-closed');
    } else {
        layout.xSidebar.classList.add('x-sidebar-closed');
    }

    xOpenSidebarNavTitleItems(flag);
    xOpenSidebarHeader(flag);
    xOpenNavItems(flag);

    layout.xSetSidebarNavHeight();

    layout.flktHeader.resize();
}

function xHideHeaderNav(flag) {
    if (flag) {
        helper.xHide(layout.xHeaderNavWrapper);
    } else {
        helper.xShow(layout.xHeaderNavWrapper);
    }
}

function xOpenLogo(flag) {
    if (flag) {
        helper.xShow(layout.xHeaderLogoWrapper);
    } else {
        helper.xHide(layout.xHeaderLogoWrapper);
    }
}

function xOpenSidebarNavTitleItems(flag) {
    layout.xSidebarTitleItems.forEach(item => xOpenSidebarNavTitleItem(item, flag));
}

function xOpenSidebarNavTitleItem(item, flag) {
    if (flag) {
        helper.xShow(item);
        let nextElement = item.nextElementSibling;
        if (nextElement !== null && nextElement !== undefined) {
            helper.xShow(nextElement);
        }
    } else {
        helper.xHide(item);
        let nextElement = item.nextElementSibling;
        if (nextElement !== null && nextElement !== undefined) {
            helper.xHide(nextElement);
        }
    }
}

function xOpenSidebarHeader(flag) {
    if (flag) {
        helper.xShow(layout.xSidebarHeader);
    } else {
        helper.xHide(layout.xSidebarHeader);
    }
}

function xOpenContentFull(flag) {
    if (flag) {
        layout.xContentWrapper.classList.remove('x-content-wrapper-full');
    } else {
        layout.xContentWrapper.classList.add('x-content-wrapper-full');
    }
}

function xOpenNavItems(flag) {
    let items = layout.xSidebarOpenSubItems();
    if (flag) {
        items.forEach(item => helper.xShow(item));
    } else {
        items.forEach(item => helper.xHide(item));
    }
}

module.exports = {
    xOpenSidebar: xOpenSidebar
};
