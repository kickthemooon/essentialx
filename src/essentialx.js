require('babel-polyfill');

// html
require('./index.html');

// css
require('./essentialx.scss');

// js
require('./js/header');
require('./js/sidebar');
require('./js/sidebarToggle');
require('./js/resize');

// font awesome icons
require('./js/fontAwesome');