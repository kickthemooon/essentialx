#Colors

The look of the template can be changed by providing 2 colors for either the header `x-hdr`  
or the sidebar `x-sbr`.

It can be an inline background style `style="backgorund: blue"`  
or via a class `class="x-hdr purple"`.

By adding a color to either `x-hdr` or `x-sbr` all the other colors  
like hover or sub-level will be calculated automatically. 

In the case of the header nav items the hover or active colors will darkened.  
In the case of the sidebar sub-items the colors will be lightened.

#Sidebar

###Sidebar nav item example

```
<ul class="x-sbr-nav">
    <li class="x-itm">
        <div class="resource">
            <a href="#">
                <div class="x-icon">
                    <i class="fas fa-tachometer-alt"></i>
                </div>
                <div class="x-title">
                    <span>Dashboard</span>
                </div>
            </a>
        </div>
    </li>
</ul>
```

###Multilevel Nav Item Example

```
<ul class="x-sbr-nav">
    <li class="x-itm">
        <div class="resource">
            <div class="x-icon">
                <i class="fas fa-newspaper"></i>
            </div>
            <div class="x-title">
                <span>Resource</span>
            </div>
            <div class="x-icon">
                <i class="fas fa-angle-down"></i>
            </div>
        </div>
        <ul class="x-sub">
            <li class="x-itm">
                <div class="resource">
                    <a href="https://google.com" target="_blank">
                        <div class="x-icon">
                            <i class="fas fa-circle"></i>
                        </div>
                        <div class="x-title">
                            <span>Overview</span>
                        </div>
                    </a>
                </div>
            </li>
            <li class="x-itm">
                <div class="resource">
                    <div class="x-icon">
                        <i class="fas fa-circle"></i>
                    </div>
                    <div class="x-title">
                        <span>Resource</span>
                    </div>
                    <div class="x-icon">
                        <i class="fas fa-angle-down"></i>
                    </div>
                </div>
                <ul class="x-sub">
                    <li class="x-itm">
                        <div class="resource">
                            <a href="#">
                                <div class="x-icon">
                                    <i class="fas fa-circle"></i>
                                </div>
                                <div class="x-title">
                                    <span>Overview</span>
                                </div>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>
```

###Make an item active

To make an item active you add the class `x-acv`

```
<li class="x-itm x-acv">
    <div class="resource">
        <a href="#">
            <div class="x-icon">
                <i class="fas fa-tachometer-alt"></i>
            </div>
            <div class="x-title">
                <span>Dashboard</span>
            </div>
        </a>
    </div>
</li>
```

The template takes care of the rest like opening up parents.

### Sidebar Header

Example sidebar header `x-sbr-hdr`

```
<div class="x-sbr-hdr">
    <div id="search" class="x-itm">
        <div class="x-search">
            <form method="get">
                <input name="q" placeholder="e.g. best movies"
                       type="text">
                <button><i class="fas fa-search"></i></button>
            </form>
        </div>
    </div>
    <div id="profile" class="x-itm">
        <a href="#">
            <div class="x-profile">
                <img src="http://images.com/user.svg"
                     alt="" width="40" height="40">
                <div class="x-profile-info">
                    <p class="greeting">Hi Bob!</p>
                    <p class="detail">
                        Last Login: <br>
                        <span>18/10/2018 18:53:16</span>
                    </p>
                </div>
            </div>
        </a>
    </div>
    <div class="x-sbr-ctrl">
        <div class="x-itm x-tgl-sbr-itms">
            <span class="x-tgl-up">
                <i class="fas fa-angle-up"></i>
            </span>
            <span class="x-tgl-down">
                <i class="fas fa-angle-down"></i>
            </span>
        </div>
        <div data-target="search" class="x-itm x-tgl x-acv">
            <i class="fas fa-search"></i>
        </div>
        <div data-target="profile" class="x-itm x-tgl">
            <i class="fas fa-user"></i>
        </div>
    </div>
</div>
```

The sidebar header is actually just another implementation of tabs.

The basic structure looks like this:

```
<div class="x-sbr-hdr">
    <div id="search" class="x-itm">
        // your content here
    </div>
    <div id="profile" class="x-itm">
        // your content here
    </div>
    <div class="x-sbr-ctrl">
        // control items to open the different contents above
        // conected via id
        <div data-target="search" class="x-itm x-tgl x-acv">
            <i class="fas fa-search"></i>
        </div>
        <div data-target="profile" class="x-itm x-tgl">
            <i class="fas fa-user"></i>
        </div>
    </div>
</div>
```

#Header

The header is mobile friendly and scrollable/swipeable.  
You can put as many items inside as you want

###Example Header item

```
<ul class="x-hdr-lst">
    <li class="x-itm">
        <a href="#">
            <span><i class="fas fa-sign-out-alt"></i></span>
        </a>
    </li>
</ul>
```

### Stream Item

```
<li data-stream-id="notifications" class="x-itm">
    <span><i class="fas fa-bell"></i></span>
    <span class="x-ups">99+</span>
</li>
```

### Drop-down Item

```
<li data-dd-box-id="ddbox" class="x-itm">
    <span><i class="fas fa-chevron-circle-down"></i></span>
</li>
```

### Stream

```
<div id="message-stream" class="x-stream">
    <div class="content scrollable">

    </div>
</div>
```

### Drop-down

```
<div id="ddbox" class="x-dd-box">
    <div class="content scrollable">
        
    </div>
</div>
```

Place your `streams` and `drop-downs` as children inside the main wrapper class `x-wpr`
