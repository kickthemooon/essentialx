const path                    = require('path');
const MiniCssExtractPlugin    = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CssNano                 = require('cssnano');

module.exports = {
    entry: {
        essentialx: [
            './src/essentialx.js'
        ]
    },
    mode: 'production',
    output: {
        filename: 'js/[name].min.js',
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {loader: 'babel-loader'}
            },
            {
                test: /\.(s*)css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            minimize: true
                        }
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.html$/,
                use: [
                    {loader: 'file-loader', options: {name: '[name].html'}},
                    {loader: 'extract-loader'},
                    {loader: 'html-loader'}
                ]
            }
        ]
    },
    plugins: [
        new OptimizeCssAssetsPlugin({
            cssProcessor: CssNano,
            cssProcessorOptions: {
                discardComments: {
                    removeAll: true
                },

            }
        }),
        new MiniCssExtractPlugin({
            filename: "css/[name].min.css"
        })
    ]
};
